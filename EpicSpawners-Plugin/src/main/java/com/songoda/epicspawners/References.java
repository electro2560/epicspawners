package com.songoda.epicspawners;

public class References {

    public static String getPrefix() {
        return EpicSpawnersPlugin.getInstance().getLocale().getMessage("general.nametag.prefix") + " ";
    }
}